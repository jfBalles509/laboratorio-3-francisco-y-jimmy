/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Admin
 */
public class CuartaConsulta {
    private String nombre;
    private String aerolinea;
    private String modelo;
    private String estado;

    public CuartaConsulta(String nombre, String aerolinea, String modelo, String estado) {
        this.nombre = nombre;
        this.aerolinea = aerolinea;
        this.modelo = modelo;
        this.estado = estado;
    }
    
    public CuartaConsulta() {
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAerolinea() {
        return aerolinea;
    }

    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
