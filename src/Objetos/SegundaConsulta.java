/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Admin
 */
public class SegundaConsulta {
    private String nombreAerolinea;
    private Integer cantidad;
    private Integer pista;

    public SegundaConsulta(String nombreAerolinea, Integer cantidad, Integer pista) {
        this.nombreAerolinea = nombreAerolinea;
        this.cantidad = cantidad;
        this.pista = pista;
    }
    
    public SegundaConsulta() {
        
    }

    public String getNombreAerolinea() {
        return nombreAerolinea;
    }

    public void setNombreAerolinea(String nombreAerolinea) {
        this.nombreAerolinea = nombreAerolinea;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPista() {
        return pista;
    }

    public void setPista(Integer pista) {
        this.pista = pista;
    }
    
    
}
