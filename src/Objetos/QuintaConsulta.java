/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Admin
 */
public class QuintaConsulta {
    private Integer id;
    private String nombre;
    private Integer pista;
    private String estado;

    public QuintaConsulta(Integer id, String nombre, Integer pista, String estado) {
        this.id = id;
        this.nombre = nombre;
        this.pista = pista;
        this.estado = estado;
    }
    
   public QuintaConsulta() {
        
    } 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPista() {
        return pista;
    }

    public void setPista(Integer pista) {
        this.pista = pista;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
