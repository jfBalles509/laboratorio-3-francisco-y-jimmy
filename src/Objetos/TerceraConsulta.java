/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Admin
 */
public class TerceraConsulta {
    private String nombre;
    private String aerolinea;
    private String rol;
    private Integer id;

    public TerceraConsulta(String nombre, String aerolinea, String rol, Integer id) {
        this.nombre = nombre;
        this.aerolinea = aerolinea;
        this.rol = rol;
        this.id = id;
    }
    
    public TerceraConsulta() {
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAerolinea() {
        return aerolinea;
    }

    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    
}
