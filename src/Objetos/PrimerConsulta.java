/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Admin
 */
public class PrimerConsulta {
    private String modelo;
    private Integer cuenta;

    public PrimerConsulta(String modelo, Integer cuenta) {
        this.modelo = modelo;
        this.cuenta = cuenta;
    }
    
    public PrimerConsulta() {
    
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getCuenta() {
        return cuenta;
    }

    public void setCuenta(Integer cuenta) {
        this.cuenta = cuenta;
    }    
}
