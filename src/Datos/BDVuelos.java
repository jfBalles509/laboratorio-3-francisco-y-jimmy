/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Objetos.CuartaConsulta;
import Objetos.PrimerConsulta;
import Objetos.QuintaConsulta;
import Objetos.SegundaConsulta;
import Objetos.SextaConsulta;
import Objetos.TerceraConsulta;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Admin
 */
public class BDVuelos {
    private static ArrayList<PrimerConsulta> primer = new ArrayList<>();
    private static ArrayList<SegundaConsulta> segunda = new ArrayList<>();
    private static ArrayList<TerceraConsulta> tercera = new ArrayList<>();
    private static ArrayList<CuartaConsulta> cuarta = new ArrayList<>();
    private static ArrayList<QuintaConsulta> quinta = new ArrayList<>();
    private static ArrayList<SextaConsulta> sexta = new ArrayList<>();
    
    private ResultSet rs = null;
    private Statement s = null;
    ConexionBD conexion = new ConexionBD();
    private Connection connection = null;
    
    public ArrayList<String> cargarGraf(String id) {
        
        String monto = id;
        
        ArrayList<String> graf = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("SELECT avion.modelo AS modelo, aerolinea.nombre_aerolinea AS nombre FROM avion, aerolinea, vuelo WHERE CAST(vuelo.precio AS int) < "+monto+" and vuelo.c_avion = avion.id and avion.pertenece_aero = aerolinea.nombre_aerolinea");
            while (rs.next()) {
                graf.add(rs.getString("modelo"));
                graf.add(rs.getString("nombre"));
                
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return graf;
    }
    
    public ArrayList<PrimerConsulta> cargarPrimer(){ //Carga los datos de la primera consulta en el array de objetos
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT av.modelo , COUNT(v.c_avion) AS cuenta
                                \tFROM avion av 
                                \tINNER JOIN vuelo v
                                \tON av.id = v.c_avion GROUP BY av.modelo""");
            while (rs.next()) {
                //System.out.println(rs.getString("modelo"));
                PrimerConsulta first = new PrimerConsulta(rs.getString("modelo"),rs.getInt("cuenta"));
                primer.add(first);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return primer;
    }
    
    public ArrayList<SegundaConsulta> cargarSegundo(){ //Carga los datos de la segunda consulta en el array de objetos
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT aerolinea.nombre_aerolinea,  COUNT(vuelo.aerolinea = aerolinea.nombre_aerolinea) AS cant_veces, vuelo.pista
                                \tFROM aerolinea
                                \tINNER JOIN vuelo
                                \tON vuelo.aerolinea = aerolinea.nombre_aerolinea
                                \tGROUP BY aerolinea.nombre_aerolinea, vuelo.pista""");
            while (rs.next()) {
                //System.out.println(rs.getInt("pista"));
                SegundaConsulta second = new SegundaConsulta(rs.getString("nombre_aerolinea"),rs.getInt("cant_veces"),rs.getInt("pista"));
                segunda.add(second);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return segunda;
    }
    
    public ArrayList<TerceraConsulta> cargarTercero(){ //Carga los datos de la tercera consulta en el array de objetos
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT tripulacion.nombre, tripulacion.trabaja_aerolinea, tripulacion.rol_tripulacion, tripulacion.id_vuelo
                                FROM tripulacion
                                WHERE tripulacion.rol_tripulacion = 'Servicio al cliente' and tripulacion.id_vuelo IS NOT NULL""");
            while (rs.next()) {
                //System.out.println(rs.getInt("id_vuelo"));
                TerceraConsulta third = new TerceraConsulta(rs.getString("nombre"),rs.getString("trabaja_aerolinea"),rs.getString("rol_tripulacion"),rs.getInt("id_vuelo"));
                tercera.add(third);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return tercera;
    }
    
    public ArrayList<CuartaConsulta> cargarCuarto(){    //Carga los datos de la cuarta consulta en el array de objetos
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT tripulacion.nombre, tripulacion.trabaja_aerolinea, avion.modelo, tripulacion.estado
                                FROM tripulacion, vuelo, avion
                                WHERE tripulacion.rol_tripulacion = 'Piloto' and tripulacion.id_vuelo IS NOT NULL and CAST(tripulacion.id_vuelo AS int) = vuelo.id_vuelo and vuelo.c_avion = avion.id""");
            while (rs.next()) {
                //System.out.println(rs.getString("estado"));
                CuartaConsulta fourth = new CuartaConsulta(rs.getString("nombre"),rs.getString("trabaja_aerolinea"),rs.getString("modelo"),rs.getString("estado"));
                cuarta.add(fourth);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return cuarta;
    }
    
    public ArrayList<QuintaConsulta> cargarQuinto(){    //Carga los datos de la quinta consulta en el array de objetos
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT vuelo.id_vuelo, vuelo.a_salida AS Nombre_Aeropuerto_Salida, vuelo.pista, pista.estado
                                FROM vuelo, pista
                                WHERE vuelo.a_salida = 'aero_Panama' and vuelo.pista = pista.num_pista and pista.estado = 'Disponible'""");
            while (rs.next()) {
                //System.out.println(rs.getString("estado"));
                QuintaConsulta fifth = new QuintaConsulta(rs.getInt("id_vuelo"),rs.getString("Nombre_Aeropuerto_Salida"),rs.getInt("pista"),rs.getString("estado"));
                quinta.add(fifth);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return quinta;
    }
    
    public ArrayList<SextaConsulta> cargarSexto(){
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("""
                                SELECT nombre,EXTRACT(YEAR from AGE(CAST(f_nacimiento AS DATE))) FROM tripulacion""");
            while (rs.next()) {
                //System.out.println(rs.getString("EXTRACT"));
                SextaConsulta sixth = new SextaConsulta(rs.getString("nombre"),rs.getInt("EXTRACT"));
                sexta.add(sixth);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return sexta;
    }
}
