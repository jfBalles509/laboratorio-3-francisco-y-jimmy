/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class ConexionBD {
    //Se realiza la conexion a la base de datos
    private Connection connection = null;

    public Connection Conexion() {
        String jdbcURL = "jdbc:postgresql://localhost:5432/Laboratorio Vuelos";
        String password = "50921001";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(jdbcURL, "postgres", password);
            if (connection != null) {
                System.out.println("Conectándo a la base de datos...");
            }
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problema conectándose a la base de datos.");
        }
        return connection;
    }
}
